// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCXpcSp9izscGo682bm5-sd7wKYYM_vaKQ",
    authDomain: "ha-app-8ca87.firebaseapp.com",
    databaseURL: "https://ha-app-8ca87.firebaseio.com",
    projectId: "ha-app-8ca87",
    storageBucket: "ha-app-8ca87.appspot.com",
    messagingSenderId: "241395814973"  
  }
};



/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
