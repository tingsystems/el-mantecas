import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from './services/firestore/firestore.service';
import { MatSnackBar } from '@angular/material';
import { copyStyles } from '@angular/animations/browser/src/util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    emailForm: FormGroup;
    isLinear = true;
    submitted = false;
    people = [];
    papers = [];
    extraPapers = [];
    assignedPapers = [];
    hasChallenge = false;
    challegers = [];
    notPaper = [];
    config: any;

    constructor(
      private formBuilder: FormBuilder, 
      private firestoreService: FirestoreService,
      private snackBar: MatSnackBar ) {

    }

    ngOnInit() {
      /** this.firestoreService.addPaper({ name: 'Sandra Valencia', kind: 0 });
      this.firestoreService.addPaper({ name: 'Invitas un six (No tecate)', kind: 1 });
      this.firestoreService.addPaper({ name: 'Armando Hernandez', kind: 0 });
      this.firestoreService.addPaper({ name: 'Eva Garcia', kind: 0 });
      this.firestoreService.addPaper({ name: 'Patricia Fajardo', kind: 0 }); */
      
      this.firestoreService.getSettings('OGG4a2Fp33oqein7nAFo').subscribe((settingsSnapshot) => {
         this.config = settingsSnapshot.payload.data();
      });

      this.firestoreService.getPapers().subscribe((papersSnapshot) => {
        this.papers = [];
          papersSnapshot.forEach((paperData: any) => {
            const data = paperData.payload.doc.data();
            this.papers.push({
              id: paperData.payload.doc.id,
              name: data.name,
              kind: data.kind
            });
        })
      });

      this.firestoreService.getPeoples().subscribe((peopleSnapshot) => {
        this.people = [];
        peopleSnapshot.forEach((peopleData: any) => {
            const data = peopleData.payload.doc.data();
            this.people.push({
              id: peopleData.payload.doc.id,
              phone: data.phone,
              password: data.password,
              name: data.name,
              hasPaper: data.hasPaper,
              papers: data.papers,
              facebook: data.facebook
            });
        });
        this.buildChallengers();
      });

      this.firestoreService.getExtraPapers().subscribe((extraPapersSnapshot) => {
        this.extraPapers = [];
        extraPapersSnapshot.forEach((paperData: any) => {
            const data = paperData.payload.doc.data();
            this.extraPapers.push({
              id: paperData.payload.doc.id,
              name: data.name,
              kind: data.kind
            });
        })
      });
  
      this.emailForm = this.formBuilder.group({
        phone: ['', Validators.required],
        password: ['', Validators.required],
      });
    }

    // convenience getter for easy access to form fields
    get f() { return this.emailForm.controls; }

    onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.emailForm.invalid) {
          return;
      }
      const people = this.people.find( people => people.phone === this.emailForm.value.phone)
      if (people === undefined) {
        this.snackBar.open('Datos incorrectos', 'ok', {
          duration: 2000,
        });
        return;
      }

      if (people.password !== this.emailForm.value.password) {
        this.snackBar.open('Contraseña incorrecta', 'ok', {
          duration: 2000,
        });
        return;
      }

      if (people.hasPaper) {
        this.snackBar.open('No sea trampos@ señor@', 'ok', {
          duration: 2000,
        });
        return;
      }
      people.hasPaper = true;
      this.firestoreService.updatePerson(people.id, people);
      this.getPaper(people);
  }

  getPaper(people) {
    const paper = this.papers[Math.floor(Math.random() * (this.papers.length - 1))];
    if (paper.name === people.name) {
      this.getPaper(people);
    } else {
      if (paper.kind === 1 && !this.hasChallenge) {
        this.hasChallenge = true;
        this.assignedPapers.push(paper);
        this.firestoreService.deletePaper(paper.id);
        this.getPaper(people);
        return;
      }
      
      if (paper.kind === 1) {
        this.getPaper(people);
        return;
      } else {
        this.assignedPapers.push(paper);
        this.firestoreService.deletePaper(paper.id);
        people.papers = this.assignedPapers;
        this.firestoreService.updatePerson(people.id, people);
        return;
      }
    }
  }

  buildChallengers() {
    for(let person of this.people) {
      let challeger = {personName: person.name};
      if(person.hasPaper) {
        for(let paper of person.papers) {
          if (paper.kind === 1){
            challeger['paper'] = paper.name;
            this.challegers.push(challeger);
          }
        }
      } else {
        this.notPaper.push(person);
      }
    }
  }
}
