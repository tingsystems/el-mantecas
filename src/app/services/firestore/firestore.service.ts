import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(private firestore: AngularFirestore) { }


  public getSettings(documentId: string) {
    return this.firestore.collection('config').doc(documentId).snapshotChanges();
  }

  public getPerson(documentId: string) {
    return this.firestore.collection('people').doc(documentId).snapshotChanges();
  }

  public updatePerson(documentId: string, data: any) {
    return this.firestore.collection('people').doc(documentId).update(data);
  }

  public getPeoples() {
    return this.firestore.collection('people').snapshotChanges();
  }

  public getPapers() {
    return this.firestore.collection('papers').snapshotChanges();
  }

  public getExtraPapers() {
    return this.firestore.collection('extraPapers').snapshotChanges();
  }

  public addPaper(data: any) {
    return this.firestore.collection('papers').add(data);
  }

  public deletePaper(documentId: string) {
    return this.firestore.collection('papers').doc(documentId).delete();
  }

}
